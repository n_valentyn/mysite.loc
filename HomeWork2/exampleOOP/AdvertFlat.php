<?php

require_once 'Advert.php';

/**
 * AdvertAvto - Класс реализующий работу с объявлениями категории Квартиры
 *
 * @author valentyn
 */
class AdvertFlat extends Advert {

    //Свойства принадлежащие категории Квартиры
    
    protected $numberOfRooms;    // Количество комнат
    protected $totalArea;        // Общая площадь
    protected $floor;            // Этаж

    /**
     * __construct при создании устанавливает все поля объявления категории Квартиры
     * 
     * @param array $advert - массив с полями объявления
     */
    function __construct(array $advert) {

        parent::__construct($advert);

        $this->numberOfRooms = $advert['numberOfRooms'];
        $this->totalArea = $advert['totalArea'];
        $this->floor = $advert['floor'];
    }
    
    /**
     * getAdvert - возвращает поля объявления категории Квартиры
     * 
     * @return array - массив с полями объявления
     */ 
    public function getAdvert() {

        return array_merge(
                parent::getAdvert(), [
            'numberOfRooms' => $this->numberOfRooms,
            'totalArea' => $this->totalArea,
            'floor' => $this->floor
                ]
        );
    }

    /**
     * setAdvert - устанавливает поля объявления категории Квартиры
     * 
     * @param array $advert - массив с полями объявления
     */
    public function setAdvert(array $advert) {

        parent::setAdvert($advert);
        
        $this->numberOfRooms = $advert['numberOfRooms'];
        $this->totalArea = $advert['totalArea'];
        $this->floor = $advert['floor'];
    }

}
