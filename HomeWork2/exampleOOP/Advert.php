<?php

require_once 'AdvertInterface.php'; //Подключение Интерфейса Объявлений

/**
 * Advert - Абстрактный класс реализующий общую логику по работе с объявлением
 *
 * @author valentyn
 */
abstract class Advert implements AdvertInterface {

    protected $creatData;     // Дата создания
    protected $updateData;    // Дата изменения
    protected $autorName;     // Имя автора
    protected $title;         // Название объявления
    protected $content;       // Текст объявления
    protected $photo;         // Фото объявления
    protected $status;        // Статус оъбявления

    /**
     * __construct при создании устанавливает общие поля объявления
     * 
     * @param array $advert - массив с полями объявления
     */
    protected function __construct(array $advert) {
        
        $this->creatData = $advert['creatData'];
        $this->updateData = $advert['updateData'];
        $this->autorName = $advert['autorName'];
        $this->title = $advert['title'];
        $this->content = $advert['content'];
        $this->photo = $advert['photo'];
        $this->status = $advert['status'];
    }

    /**
     * getAdvert - возвращает общие поля объявления
     * 
     * @return array - массив с общими полями объявления
     */
    public function getAdvert() {

        return [
            'creatData' => $this->creatData,
            'updateData' => $this->updateData,
            'autorName' => $this->autorName,
            'title' => $this->title,
            'content' => $this->content,
            'photo' => $this->photo,
            'status' => $this->status
        ];
    }

    /**
     * setAdvert - устанавливает общие поля объявления
     * 
     * @param array $advert - массив с полями объявления
     */
    public function setAdvert(array $advert) {
        
        $this->creatData = $advert['creatData'];
        $this->updateData = $advert['updateData'];
        $this->autorName = $advert['autorName'];
        $this->title = $advert['title'];
        $this->content = $advert['content'];
        $this->photo = $advert['photo'];
        $this->status = $advert['status'];
    }

}
