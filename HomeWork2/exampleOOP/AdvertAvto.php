<?php

require_once 'Advert.php';

/**
 * AdvertAvto - Класс реализующий работу с объявлениями категории Автомобили
 *
 * @author valentyn
 */
class AdvertAvto extends Advert {
    
    //Свойства принадлежащие  категории Автомобилю
    
    protected $brand;      // Производитель
    protected $model;      // Модель
    protected $year;       // Год выпуска
    protected $mileage;    // Пробег
    
    /**
     * __construct при создании устанавливает все поля объявления категории Автомобили
     * 
     * @param array $advert - массив с полями объявления
     */
    function __construct(array $advert) {

        parent::__construct($advert);

        $this->brand = $advert['brand'];
        $this->model = $advert['model'];
        $this->year = $advert['year'];
        $this->mileage = $advert['mileage'];
    }

    /**
     * getAdvert - возвращает поля объявления категории Автомобили
     * 
     * @return array - массив с полями объявления
     */ 
    public function getAdvert() {

        return array_merge(
                parent::getAdvert(), [
            'brand' => $this->brand,
            'model' => $this->model,
            'year' => $this->year,
            'mileage' => $this->mileage
                ]
        );
    }

    /**
     * setAdvert - устанавливает поля объявления категории Автомобили
     * 
     * @param array $advert - массив с полями объявления
     */
    public function setAdvert(array $advert) {

        parent::setAdvert($advert);

        $this->brand = $advert['brand'];
        $this->model = $advert['model'];
        $this->year = $advert['year'];
        $this->mileage = $advert['mileage'];
    }

}
