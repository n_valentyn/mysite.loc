<?php

require_once 'Advert.php'; 

/**
 * AdvertOther - Класс реализующий работу с объявлениями категории Другие
 *
 * @author valentyn
 */
class AdvertOther extends Advert {

    /**
     * __construct при создании устанавливает все поля объявления категории Другие
     * 
     * @param array $advert - массив с полями объявления
     */
    function __construct(array $advert) {
        
        parent::__construct($advert);
    }

    /**
     * getAdvert - возвращает поля объявления категории Другие
     * 
     * @return array - массив с полями объявления
     */ 
    public function getAdvert() {
        
        return parent::getAdvert();
    }

    /**
     * setAdvert - устанавливает поля объявления категории Другие
     * 
     * @param array $advert - массив с полями объявления
     */
    public function setAdvert(array $advert) {

        parent::setAdvert($advert);
    }

}
