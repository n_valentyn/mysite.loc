<?php
//Подключение необходимых класов
require_once 'AdvertAvto.php';
require_once 'AdvertFlat.php';
require_once 'AdvertOther.php';

require_once 'ManagerAdvert.php';

//Запись даных в $data
$data = require_once 'DataBase.php';

//Создание объектов объявлений разных категорий
$advertAvto = new AdvertAvto($data['Avto']);
$advertFlat = new AdvertFlat($data['Flat']);
$advertOther = new AdvertOther($data['Other']);

//Создание объекта менеджера по работе с объявлениями
$manager = new ManagerAdvert();

//Получение информации с помощью менеджера по каждом объявлении в соответствующий масив
$arrayAvto = $manager->getInfo($advertAvto);
$arrayFlat = $manager->getInfo($advertFlat);
$arrayOther = $manager->getInfo($advertOther); 

//Вывод масивов с обявлениями
echo '<pre>';
print_r($arrayAvto);
print_r($arrayFlat);
print_r($arrayOther);

//Изменение полей обявлений
$arrayAvto['content'] = 'Продам свою БМВ';
$arrayAvto['year'] = 1994;

$arrayFlat['totalArea'] = 79;
$arrayFlat['photo'] = 'photo/Flat/54638.jpg';

$arrayOther['status'] = 0;

//Запись полей объявлений в объекты с помощью менеджера
$manager->setInfo($advertAvto, $arrayAvto);
$manager->setInfo($advertFlat, $arrayFlat);
$manager->setInfo($advertOther, $arrayOther);

//Вывод масивов с обявлениями
echo '<br><hr><br>';
print_r($manager->getInfo($advertAvto));
print_r($manager->getInfo($advertFlat));
print_r($manager->getInfo($advertOther));
