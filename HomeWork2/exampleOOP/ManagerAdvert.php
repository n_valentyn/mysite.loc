<?php
require_once 'AdvertInterface.php';

/**
 * ManagerAdvert - класс для работы с объявлениями разных категорий
 *
 * @author valentyn
 */
class ManagerAdvert {
    
    /**
     * getInfo - метод возвращает масив полей Объявления
     * 
     * @param AdvertInterface $advert - объект Объявления
     * @return array - массив с полями Обявления
     */
    public function getInfo(AdvertInterface $advert) {
        return $advert->getAdvert(); 
    }
    
    /**
     * setInfo - метод устанавливает поля обявления 
     * 
     * @param AdvertInterface $advert - объект Объявления
     * @param array $data - массив с полями объявления 
     */
    public function setInfo(AdvertInterface $advert, array $data) {
        $advert->setAdvert($data);
    }
}
