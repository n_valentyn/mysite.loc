<?php

/**
 * AdvertInterface - интерфейс для работы с Объявлениями
 * 
 * @author valentyn
 */
interface AdvertInterface {
    
    /**
     * Возвращает массив полей Объявления
     * 
     * @return array - массив полей
     */
    public function getAdvert();
    
    /**
     * Устанавливает поля в Объявлении
     * 
     * @param array $advert -массив полей
     */
    public function setAdvert(array $advert);
}
